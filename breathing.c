
#include "breathing.h"

#include <stdbool.h>
#include <math.h>

#include "mean_average_filter.h"
#include "axis_and_angle.h"
#include "average.h"
#include "mean_axis.h"
#include "activity_filter.h"
#include "math_helper.h"

//#include "arm_math.h"

mean_average_filter input_filter;
axis_and_angle_filter aaa_filter;
mean_axis_filter axis_filter;
average_filter average;
activity_filter activity;

void BRG_init(breathing_filter* filter)
{

	// mean average filter
	MAF_init(&input_filter);
	// axis and angle filter
	AAA_init(&aaa_filter);
	// mean axis filter
	MAX_init(&axis_filter);
	// average filter
	AVG_init(&average);
	// activity filter
	//ACT_init(&activity);

	filter->valid = false;
	filter->sample_rate_valid = false;

}

void BRG_update(double value[3], breathing_filter* filter)
{

	if (filter->sample_rate_valid == false)
	{
		filter->valid = false;
		return;
	}
    
    if (isnan(value[0]) || isnan(value[1]) || isnan(value[2]))
    {
        filter->valid = false;
        return;
    }

	double accel[3];
	accel[0] = value[0];
	accel[1] = value[1];
	accel[2] = value[2];

	filter->valid = false;
    
    // activity (movement detection)
	ACT_update(accel,&activity);
    
	if (activity.valid == false)
	{
		filter->valid = false;
		return;
	}
    
	filter->activity = activity.max;
    
	if (activity.max > ACTIVITY_CUTOFF)
	{
		filter->valid = false;
        filter->bs = NAN;
		return;
	}

	/* mean average filter */
	MAF_update(accel, &input_filter);

	if (input_filter.valid == false)
	{
		filter->valid = false;
		return;
	}

	vector_copy_dbl(input_filter.value,accel);

	/* normalize */
	//double norm_accel[3];
	//normalize_int_to_float(accel, axis); // using arm_f32 functions

    normalize(accel);

	/* convert to q31 */
	//q31_t q31_axis[3];
	//arm_float_to_q31(axis,q31_axis,3);

	AAA_update(accel, &aaa_filter);

	if (aaa_filter.valid == false)
	{
		filter->valid = false;
		return;
	}

	/* the activity code needs optimising - it massively increases computation time because there is a lot of floating point operations */

	
	//arm_q31_to_float(aaa_filter.value,axis,3);

	MAX_update(aaa_filter.value,&axis_filter);

	if (axis_filter.valid == false)
	{
		filter->valid = false;
		return;
	}

	/* final breathing signal calculation */
	double final_bs;

	//arm_dot_prod_f32(axis_filter.value,accel,3,&final_rate);
    final_bs = dot(aaa_filter.value, axis_filter.value);
	//arm_scale_f32(&final_rate,filter->sample_rate,&final_rate,3);
    final_bs = final_bs * filter->sample_rate * 10.0f;
    

	AVG_update(final_bs,&average);

	if (average.valid == false)
	{
		filter->valid = false;
		return;
	}

	// update the rate value
	filter->bs = average.value;
	filter->valid = true;

}
